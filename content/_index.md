Este é um site dedicado a trazer tutoriais de origami.
Origami é uma arte milenar que consiste em dobrar papel para fazer diferentes esculturas, como animais e objetos.
Algumas dessas obras são simples, enquanto outras podem ser mais dinâmicas, como aviões e balões.
Clique em tutoriais para ver alguns exemplos.
